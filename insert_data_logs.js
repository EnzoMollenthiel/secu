const util = require('util');
const prompt = require('prompt');
const mysql = require('mysql');

const fs = require('fs')
const path = require('path');

const directoryScans = path.join(__dirname, 'scanResults');
const file = directoryScans + '/29-03-2018/jaja.csv';
const allFolder = [];

const schema = {
    properties: {
        username: {
            required: true
        },
        password: {
            hidden: true
        }
    }
};

prompt.start();

prompt.get(schema, function (err, result) {

    const connection = mysql.createConnection({
        host: 'localhost',
        user: result.username,
        password: result.password,
        database: 'bdd_scan'
    });

    connection.connect();

    let scannedIp = [];
    let scanId = null;
    let myArchiveResults = null; // creer variable
    const currentDate = new Date();

    fs.readdir(directoryScans, function (err, folders) {
        if (err) {
            console.log('Unable to scan directory: ' + err);
        }
        folders.forEach(function (folder) {
            allFolder.push(folder);
        });

        const lastDirectory = new Date(Math.max.apply(null, allFolder.map(function (element) {
            return new Date(element);
        }))).toISOString();

        const directoryScanDate = path.join(directoryScans, lastDirectory);

            fs.readdir(directoryScanDate, async function (err, files) {

            if (err) {
                console.log('Unable to scan directory: ' + err);
            }

            await connection.query('SELECT COUNT(DISTINCT IP_ADDRESS) as TOTAL_IP FROM VULN', async function(err, scanResults) {
                if(err) throw err; 
                
                await connection.query('INSERT INTO SCAN(SCAN_DATE, TOTAL_IP, SCANNED_IP) VALUES(?,?,?)', [currentDate, scanResults[0].TOTAL_IP, 0], function(err, r) { //changer ici
                    if(err) throw err;

                    scanId = r.insertId; //changer ici
                });
            });

                let filesLength = files.length;
                files.forEach(async (file) => { 

                    //enregistrer archive
                    await fs.readFile(directoryScanDate + '/' + file, (err, data) => {

                        if (err) {
                            throw err;
                        }
                        const textFile = data.toString();
                        const lines = textFile.split('\n');

                        const splittedLine = lines[0].split(',');
                        const archive = [
                            currentDate,
                            splittedLine[0],
                            splittedLine[2],
                            splittedLine[3]
                        ];
                        connection.query('SELECT COUNT(*) AS COUNT FROM ARCHIVE WHERE MD5_SUM = ?', splittedLine[3], function(error, archiveCount, fields) {
                            if (archiveCount[0].COUNT == 0) {
                                connection.query('INSERT INTO ARCHIVE(ARCHIVE_DATE, SCAN_TYPE, LOG_FILE, MD5_SUM) VALUES (?, ?, ?, ?)',
                                    archive,
                                    function (error, archiveResults, fields) {
                                    if (error) throw error;

                                    let linesLength = lines.length;
                                    lines.forEach(async (line) => {

                                        argumentsLine = line.split(',');
                                        const vulnerabilityFind = {
                                            SCAN_NAME: argumentsLine[0],
                                            SCAN_DATE: argumentsLine[1],
                                            LOG_FILE: argumentsLine[2],
                                            MD5_SUM: argumentsLine[3],
                                            IPADDRESSDNS: argumentsLine[4],
                                            PORT: argumentsLine[5],
                                            CVSS: argumentsLine[6],
                                            BREACH: argumentsLine[7],
                                            //                        DESCRIPTION: argumentsLine[8],
                                        };

                                        await connection.query('SELECT ID_VULN, RECURRENCE FROM VULN WHERE IP_ADDRESS = ? AND BREACH = ? AND CORRECTION_DATE IS NULL',
                                            [vulnerabilityFind.IPADDRESSDNS, vulnerabilityFind.BREACH], function (error, vulnResults, fields) {
                                            if (error) throw error;

                                            if (vulnResults[0] == null || vulnResults[0].ID_VULN == null) {

                                                let urgency = null;
                                                let severity = null;
                                                if (vulnerabilityFind.CVSS < 1) { //changer ici
                                                    urgency = 5;
                                                    severity = 5
                                                } 
                                                else if (vulnerabilityFind.CVSS >= 1 && vulnerabilityFind.CVSS < 4) { //changer ici
                                                    urgency = 4;
                                                    severity = 4
                                                }
                                                else if (vulnerabilityFind.CVSS >= 4 && vulnerabilityFind.CVSS < 7) { //changer ici
                                                    urgency = 3;
                                                    severity = 3;
                                                }
                                                else if (vulnerabilityFind.CVSS >= 7 && vulnerabilityFind.CVSS < 9) { //changer ici
                                                    urgency = 2;
                                                    severity = 2;
                                                }
                                                else if (vulnerabilityFind.CVSS >= 9) { //changer ici
                                                    urgency = 1;
                                                    severity = 1;
                                                };

                                                const vulnerability = [
                                                    vulnerabilityFind.IPADDRESSDNS,
                                                    vulnerabilityFind.CVSS,
                                                    vulnerabilityFind.BREACH,
                                                    vulnerabilityFind.PORT,
                                                    null,
                                                    currentDate,
                                                    urgency,
                                                    2, //peux pass etre null donc j'ai mis 2
                                                    severity,
                                                    null,
                                                    1,
                                                    null,  //ajout ici pour faire 12  
                                                    scanId
                                                ];

                                                connection.query(`
                                                INSERT INTO VULN(IP_ADDRESS, CVSS, BREACH, PORT, CORRECTIVE, APPEARANCE_DATE, URGENCY, AUTHENTICITY, SEVERITY, CORRECTION_DATE, RECURRENCE, MAINTAINABLE, ID_SCAN)
                                                VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)   
                                                `, vulnerability, function (error, insertVulnResults, fields) {
                                                        if (error) throw error;
                                                    });
                                            } else {

                                                const recurrence = vulnResults[0].RECURRENCE +1 //ajout const + rectification partout orthographe recurrence

                                                connection.query(`
                                                UPDATE VULN SET RECURRENCE = ? WHERE ID_VULN = ?
                                                `, [recurrence, vulnResults[0].ID_VULN], function (error, insertVulnResults, fields) {
                                                        if (error) throw error;
                                                    });
                                            }

                                            connection.query('SELECT * FROM VULN WHERE IP_ADDRESS = ? AND BREACH = ? AND CORRECTION_DATE IS NULL', 
                                            [vulnerabilityFind.IPADDRESSDNS, vulnerabilityFind.BREACH],
                                            function (err, newVulnResults) { //ajout car insert ne renvoie pas la donné
                                                if (err) throw err;

                                                vulnResults = newVulnResults;

                                                connection.query('SELECT * FROM ARCHIVE WHERE MD5_SUM = ?', [splittedLine[3]], function (err, newArchiveResults) { //ajout car insert ne renvoie pas la donné
                                                    if (err) throw err;

                                                    if (!scannedIp.includes(vulnResults[vulnResults.length-1].ID_VULN)) scannedIp.push(vulnResults[vulnResults.length-1].ID_VULN);
                                                    
                                                    connection.query('INSERT INTO ARCHIVE_VULN(ID_VULN, ID_LOG) VALUES(?,?)', [vulnResults[vulnResults.length - 1].ID_VULN, newArchiveResults[newArchiveResults.length-1].ID_LOG], function (error, archVulnResults, fields) { //corriger ID par ID_vuln et rajout de [0] !!!!! ne marche pas deux fois d'affilé car ce sont des clé primaire, il faut changer en foreign key
                                                        if (error) throw error;
                                                    
                                                    if(file == files[files.length-1]){

                                                        connection.query('UPDATE SCAN SET SCANNED_IP = ? WHERE ID_SCAN = ?', [scannedIp.length, scanId])
                                                        connection.query(`
                                                        UPDATE VULN VU  SET CORRECTION_DATE = ? 
                                                        WHERE VU.ID_VULN IN (          
                                                            SELECT 
                                                            V.ID_VULN
                                                            FROM
                                                            (SELECT * FROM VULN) V
                                                                JOIN
                                                            ARCHIVE A ON ARCHIVE_DATE = (SELECT 
                                                                    SCAN_DATE
                                                                FROM
                                                                    SCAN
                                                                ORDER BY SCAN_DATE DESC
                                                                LIMIT 1 , 1)
                                                                JOIN
                                                            ARCHIVE_VULN AV ON AV.ID_VULN = V.ID_VULN
                                                            WHERE
                                                            BREACH NOT IN (SELECT 
                                                                    BREACH
                                                                FROM
                                                                    (SELECT * FROM VULN) V
                                                                        JOIN
                                                                    ARCHIVE A ON ARCHIVE_DATE = (SELECT 
                                                                            SCAN_DATE
                                                                        FROM
                                                                            SCAN
                                                                        ORDER BY SCAN_DATE DESC
                                                                        LIMIT 1)
                                                                        JOIN
                                                                    ARCHIVE_VULN AV ON AV.ID_VULN = V.ID_VULN
                                                                WHERE
                                                                    AV.ID_LOG = A.ID_LOG
                                                                GROUP BY V.ID_VULN)
                                                                AND AV.ID_LOG = A.ID_LOG
                                                            GROUP BY V.ID_VULN
                                                            )`, [currentDate]);
                                                    }
                                                    });
                                                });
                                            });
                                        })
                                    });
                                });
                            }
                        });
                    });
            });
        });
    });
});

